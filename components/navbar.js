import React, { useState } from "react";
import { Transition } from "@headlessui/react";

// export default function Navbar() {
//   const [style, setStyle] = useState("navbar1");

function Nav() {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div>
      <div className=" flex items-center justify-center">
        <div className="navbar bg-neutral text-neutral-content rounded-box p-2 w-5/6">
          <div className="flex-1">
            <a className="btn btn-ghost normal-case text-xl">daisyUI</a>
          </div>
          <div className="flex-none">
            <ul className="hidden menu menu-horizontal p-0">
              <li>
                <a>Item 1</a>
              </li>
              <li>
                <a>Item 3</a>
              </li>
            </ul>
          </div>
        </div>
        <div className="flex  justify-end md:hidden ">
          <ul className="menu bg-base-100 w-56 p-2 rounded-box">
            <li>
              <a>Item 1</a>
            </li>
            <li>
              <a>Item 2</a>
            </li>
            <li>
              <a>Item 3</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Nav;
